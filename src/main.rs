mod lib;

fn main() {
    run();
}

fn run() {
    use lib::lookup;
    let arg = useful_macro::args!();
    if arg.len() < 1 {
        println!("look_ip is dns lookup cli");
        println!("look_ip just need a domain argument");
        println!("Example:");
        println!("look_ip deno.land");
    } else if arg.len() == 1 {
        let url = useful_macro::vec_element_clone!(arg, 0);
        let re = lookup(&url);
        for ip in re.iter() {
            println!("{}  {}", ip, url);
        }
    } else {
        println!("look_ip is dns lookup cli");
        println!("look_ip just need a domain argument");
        println!("Example:");
        println!("look_ip deno.land");
    }
}
