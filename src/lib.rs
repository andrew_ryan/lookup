#[allow(warnings)]
pub fn lookup(url: &str) -> Vec<String> {
    use std::net::*;
    use tokio::runtime::Runtime;
    use trust_dns_resolver::config::*;
    use trust_dns_resolver::TokioAsyncResolver;

    // We need a Tokio Runtime to run the resolver
    //  this is responsible for running all Future tasks and registering interest in IO channels
    let mut io_loop = Runtime::new().unwrap();

    // Construct a new Resolver with default configuration options
    let resolver = io_loop
        .block_on(async {
            TokioAsyncResolver::tokio(ResolverConfig::default(), ResolverOpts::default())
        })
        .expect("failed to connect resolver");

    // Lookup the IP addresses associated with a name.
    // This returns a future that will lookup the IP addresses, it must be run in the Core to
    //  to get the actual result.
    let lookup_future = resolver.lookup_ip(url);

    // Run the lookup until it resolves or errors
    let mut response = io_loop.block_on(lookup_future).unwrap();

    let mut re = vec![];
    for address in response.iter() {
        re.push(address.to_string());
    }
    re
}
