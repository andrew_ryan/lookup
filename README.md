## look_ip is dns lookup cli
look_ip just need a domain argument

Example:
```
#get deno.land dns ip
look_ip deno.land #return the blow ip address
34.120.54.55  deno.land
```

![look_ip.gif](https://gitlab.com/andrew_ryan/lookup/-/raw/main/lookup.gif)


## Install
```
cargo install look_ip
```